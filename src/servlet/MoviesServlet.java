package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Peliculas;
import persistence.PeliculasDAO;

/**
 * Servlet implementation class MoviesServlet
 */
@WebServlet({ "/movies", "/movies/*" })
public class MoviesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PeliculasDAO peliculaDAO;
    
	public void init() {
		String jdbcURL = getServletContext().getInitParameter("jdbcURL");
		String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");
		try {

			peliculaDAO = new PeliculasDAO(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MoviesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/mostrar.jsp");
		dispatcher.forward(request, response);
		List<Peliculas> listaPeliculas = null;
		try {
			listaPeliculas = PeliculasDAO.listarPeliculas();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("lista", listaPeliculas);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Hola Servlet..");
		doGet(request, response);
	}

	private void home (HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException{
		//mostrar(request, response);
		RequestDispatcher dispatcher= request.getRequestDispatcher("/WEB-INF/view/index.jsp");
		dispatcher.forward(request, response);
	}	
	
	private void movies(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException , ServletException{
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/mostrar.jsp");
		List<Peliculas> listaPeliculas= PeliculasDAO.listarPeliculas();
		request.setAttribute("lista", listaPeliculas);
		dispatcher.forward(request, response);
	}	
	
	private String[] getUriPieces(HttpServletRequest request) {
        String uri = request.getRequestURI();
        System.out.println(uri);

        String[] pieces = uri.split("/");
        int i = 0;
        for (String piece : pieces) {
            System.out.println(i + ": " + piece);
            i++;
        }
        return pieces;
    }

}