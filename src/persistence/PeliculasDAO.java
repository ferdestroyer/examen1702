package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Conexion;
import model.Peliculas;

/*
 * @autor: Ferdestroyer
 */

public class PeliculasDAO {
	private static Conexion con;
	private static Connection connection;

	public PeliculasDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {
		System.out.println(jdbcURL);
		con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
	}

	// listar todos los productos
		public static List<Peliculas> listarPeliculas() throws SQLException {

			List<Peliculas> listaPeliculas = new ArrayList<Peliculas>();
			String sql = "SELECT * FROM movies";
			con.conectar();
			connection = con.getJdbcConnection();
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			while (resulSet.next()) {
				int id = resulSet.getInt("id");
				String title = resulSet.getString("title");
				String director = resulSet.getString("director");
				int year = resulSet.getInt("year");
				Peliculas pelicula = new Peliculas(id, title, director, year);
				listaPeliculas.add(pelicula);
			}
			con.desconectar();
			return listaPeliculas;
		}
}
