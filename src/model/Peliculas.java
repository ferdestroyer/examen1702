package model;

/*
 * @autor: Ferdestroyer
 */

public class Peliculas {
	private int id;
	private String title;
	private String director;
	private int year;
	
	public Peliculas(int id, String title, String director, int year) {
		this.id = id;
		this.title = title;
		this.director = director;
		this.year = year;
	}
	//getters y setters
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}


}
