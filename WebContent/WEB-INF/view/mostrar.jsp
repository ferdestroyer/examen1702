<%@page import="model.Peliculas"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrar Peliculas</title>
</head>
<body>
	<h1>Lista de peliculas</h1>
	<table>
		<tr>
			<td><a href="adminPelicula?action=index" >Ir al menú</a> </td>
		</tr>
	</table>
	
	<table border="1" width="100%">
		<tr>
		 <td> ID</td>
		 <td> TITULO</td>
		 <td> DIRECTOR</td>
		 <td>AÑO</td>
		 <td colspan=2>ACCIONES</td>
		</tr>
		<c:forEach var="pelicula" items="${lista}">
			<tr>
				<td><c:out value="${pelicula.id}"/></td>
				<td><c:out value="${pelicula.title}"/></td>
				<td><c:out value="${pelicula.director}"/></td>
				<td><c:out value="${pelicula.year}"/></td>
				<td><a href="adminArticulo?action=showedit&id=<c:out value="${articulo.id}" />">Editar</a></td>
				<td><a href="adminArticulo?action=eliminar&id=<c:out value="${articulo.id}"/>">Eliminar</a> </td>				
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>